"""
Contains implementations for various helper functions to aid in working with SHMS dataset:
Dependencies: numpy, pdc.py

collate()          - converts parallel TBranch objects into lists of tracks
time_to_dist()     - converts drift time into distance from wire
point_line_dist()  - calculates 2D euclidian distance from a point to a line
hit_to_2d()        - converts a wire hit into equation of wire in 2D space
intersect()        - computes intersection point of two 2D lines
hit_to_3d()        - converts a wire hit into equation of wire in 3D space
track_to_2d_pos()  - calculates the position at which the particle hit the sensor plane
track_to_heatmap() - calculates a heatmap of cost function values for a single track

"""


import numpy as np
from pdc import particle_drift_chamber as PDC

EPSILON = 10 ** -7  # small value to prevent division by zero


def collate(*args, validate=True, preserve_groups=True, drop_empty=False):
    """Takes as input an arbitrary number of parallel TBranch objects and
    returns a list of numpy arrays, with each row containing the parallel
    elements of the TBranches.
    
    If validate = True, the function will perform a pre-pass over the data
    to make sure the TBranch objects are, in fact, parallel.
    This SHOULD NOT be set to False unless you know exactly what you're doing
    because not performing this check can crash the python environment.
    
    If preserve_groups = True, function will return the collated branches
    with the original rows intact; else, it will return a flattened version
    of the arguments.
    
    If drop_empty = True, rows which are entirely empty will not be included
    in output. If preserve_groups = False, drop_empty has no effect.
    
    Expects numpy to have been imported as np.
    """
    
    # Convert TBranch objects into jagged arrays for faster processing
    branches = [branch.array() for branch in args]
    
    # Don't check if TBranch objects are parallel when there is only one of them
    if len(branches) < 2:
        validate = False
    
    # Make sure TBranch objects are actually parallel (don't trust the user)
    if validate:
        for i, branch in enumerate(branches):
            if len(branch) != len(branches[0]):
                raise ValueError("TBranch object dimensions do not match: " + str([len(branch) for branch in branches])[1:-1])
            for j, track in enumerate(branch):
                if len(track) != len(branches[0][j]):
                    raise ValueError("TBranch object dimensions do not match: first error at item {0} of arg {1}".format(j, i))
                
    # Flatten jagged arrays and reshape (using numpy for speed)
    flattened_jagged_arrays = np.asarray([[item for sublist in branch for item in sublist] for branch in branches])
    hits = flattened_jagged_arrays.transpose()
    
    if not preserve_groups:
        return list(hits)
    
    # The hit groupings were removed by the flattening; group the hits again manually
    tracks = [None] * len(branches[0])  # all the branches should have the same length; use 0th for simplicity
    j = 0  # position within flattened list
    for i, track in enumerate(branches[0]):
        tracks[i] = hits[j:j+len(track)]
        j += len(track)
    
    # Remove empty tracks
    if drop_empty:
        tracks = [track for track in tracks if len(track) > 0]
    
    # Done
    return tracks


def time_to_dist(t, a=1):
    """Function to convert drift times into distance from wire in centimeters
    
    t - drift time measurement
    a - scale factor
    """
    
    result = abs(a*(t/PDC.max_time)) * (PDC.pitch/2)  # linear relationship
    return result if result < (PDC.pitch/2) else (PDC.pitch/2)


def point_line_dist(point, line):
    """Returns the 2D distance from point to line
    point = (x, y)
    line = ((x1, x2), (y1, y2))
    
    Intended to be used in conjunction with hit_to_2d().
    """
    
    x0 = point[0]
    y0 = point[1]
    
    x1 = line[0][0]
    y1 = line[1][0]
    x2 = line[0][1]
    y2 = line[1][1]
    
    denom = np.sqrt(((x2 - x1)*(x2 - x1)) + ((y2 - y1)*(y2 - y1)))
    
    # if line has length == 0, return euclidian distance between point and first endpoint of line
    if denom < EPSILON:
        return np.sqrt(((x1 - x0)*(x1 - x0)) + ((y1 - y0)*(y1 - y0)))
    
    # For a description of how this works, see the Wolfram Mathworld article on point-line distance
    return abs(((x2 - x1)*(y1 - y0)) - ((x1 - x0)*(y2 - y1))) / denom


def hit_to_2d(hit, half_plane=0, shear=False):
    """Takes in data corresponding to a PDC wire hit and outputs the endpoints of
    the wire on the 2D plane containing the wire.
    
    half_plane: integer +-1 multiplier to determine whether to add or subtract time value from wirenum
    
    Returns a tuple of two groups. The first of these contains the x-coordinates of
    the endpoints, while the second contains the y-coordinates. These are intended
    to be used with Matplotlib's plot() function to visualize the wire hit.
    """
    
    # Conversion
    plane = int(hit[0]) - 1  # plane numbering starts at 1, while python indices start at 0
    time = hit[1]
    wirenum = hit[2] + half_plane * time_to_dist(time, a=5)
    dcnum = 0 if plane < 6 else 1
    
    # Initialization
    x1 = x2 = y1 = y2 = 0
    
    # X plane case
    if (plane-(6*dcnum))==2 or (plane-(6*dcnum))==3:
        x1 = -PDC.pcbwidth/2
        y1 = (PDC.central_wire[plane] - wirenum) * PDC.pitch

        x2 = PDC.pcbwidth/2
        y2 = (PDC.central_wire[plane] - wirenum) * PDC.pitch
    
    # V plane case
    if (plane-(6*dcnum))==4 or (plane-(6*dcnum))==5:
        x1 = -PDC.pcbwidth/2
        y1 = (PDC.central_wire[plane] - wirenum - PDC.wire_offset) * PDC.pitch / np.cos(PDC.uslope * PDC.raddeg)
        
        y2 = PDC.pcbheight/2
        x2 = ((y2 - y1) / np.tan(PDC.uslope * PDC.raddeg)) + x1
        
        # Crop lines to boundaries of drift chamber
        if y1 < -PDC.pcbheight/2:
            y1 = -PDC.pcbheight/2
            x1 = ((y1 - y2) / np.tan(PDC.uslope * PDC.raddeg)) + x2
        if x2 > PDC.pcbwidth/2:
            x2 = PDC.pcbwidth/2
            y2 = np.tan(PDC.uslope * PDC.raddeg) * (x2 - x1) + y1
        
    # U plane case
    if (plane-(6*dcnum))==0 or (plane-(6*dcnum))==1:
        x1 = -PDC.pcbwidth/2
        y1 = (PDC.central_wire[plane] - wirenum - PDC.wire_offset) * PDC.pitch / np.cos(PDC.uslope * PDC.raddeg)
        
        y2 = PDC.pcbheight/2
        x2 = ((y2 - y1) / np.tan(PDC.uslope * PDC.raddeg)) + x1
        
        # Crop lines to boundaries of drift chamber
        if y1 < -PDC.pcbheight/2:
            y1 = -PDC.pcbheight/2
            x1 = ((y1 - y2) / np.tan(PDC.uslope * PDC.raddeg)) + x2
        if x2 > PDC.pcbwidth/2:
            x2 = PDC.pcbwidth/2
            y2 = np.tan(PDC.uslope * PDC.raddeg) * (x2 - x1) + y1
        
        # U and V are identical except for a 180 rotation about vertical axis
        x1 = -x1
        x2 = -x2
        
    # Add wire chamber offsets
    x1 += PDC.xpos_offset[plane]
    x2 += PDC.xpos_offset[plane]
    y1 += PDC.ypos_offset[plane]
    y2 += PDC.ypos_offset[plane]
    
    if shear:
        # Create vectors from points
        p1 = np.matrix( [[x1], [y1]] )
        p2 = np.matrix( [[x2], [y2]] )
        # Create shear matrix
        mtx_shear = np.matrix( [[1, -1/np.sqrt(3)], [0, 1]] )
        # Transform vectors
        p1 = np.matmul(mtx_shear, p1)
        p2 = np.matmul(mtx_shear, p2)
        # Convert back to points
        x1 = np.squeeze(np.asarray(p1))[0]
        y1 = np.squeeze(np.asarray(p1))[1]
        x2 = np.squeeze(np.asarray(p2))[0]
        y2 = np.squeeze(np.asarray(p2))[1]
    
    return ((x1, x2), (y1, y2))


def intersect(line1, line2):
    """Takes in two lines in the form of tuples of points. Each argument
    should be a tuple containing a pair of x-coordinates and a pair of y-
    coordinates. Returns the intersection point (u,v) of the two lines or,
    if the lines are parallel, returns None. See the Wikipedia page on
    line-line intersection for an explanation of the mathematics used here.
    
    Designed to be used in conjunction with hit_to_2d()
    """
    
    x1 = line1[0][0]
    y1 = line1[1][0]
    x2 = line1[0][1]
    y2 = line1[1][1]
    x3 = line2[0][0]
    y3 = line2[1][0]
    x4 = line2[0][1]
    y4 = line2[1][1]
    
    denominator = ((x1 - x2)*(y3 - y4)) - ((y1 - y2)*(x3 - x4))
    
    if abs(denominator) < EPSILON:
        return None  # if denominator is zero, lines are parallel
    
    u = ((((x1*y2) - (y1*x2))*(x3 - x4)) - ((x1 - x2)*((x3*y4) - (y3*x4)))) / denominator
    v = ((((x1*y2) - (y1*x2))*(y3 - y4)) - ((y1 - y2)*((x3*y4) - (y3*x4)))) / denominator
    
    return (u,v)


def hit_to_3d(hit):
    """Takes in data corresponding to a PDC wire hit and outputs the equation
    of the wire in 3D space.
    
    Returns a tuple of tuples. The first is a vector representing the 3D
    location of one end of the wire; the second is a vector representing the
    direction/length of the wire. These are intended to be used with
    Matplotlib's 3D quiver() function in order to visualize the wire hit.
    """
    
    # Get 2D line from hit
    line = hit_to_2d(hit)
    
    # Convert float arguments to ints
    plane = int(hit[0]) - 1  # plane numbering starts at 1, while python indices start at 0
    wirenum = int(hit[2])
    dc_num = 0 if (plane < 6) else 1  # drift chambers 1 and 2 map to 0 and 1, respectively
    
    # Create direction and position vectors
    a = np.asarray([line[0][0], line[1][0], PDC.zpos_offset[plane]])
    b = np.asarray([line[0][1], line[1][1], PDC.zpos_offset[plane]])
    
    v  = b - a
    r0 = a
    
    v = v.reshape((3,1))
    r0 = r0.reshape((3,1))
    
    # Retrieve values from geometry data and convert to radians
    if plane < 6:
        roll  = PDC.orientation[0] * PDC.raddeg
        pitch = PDC.orientation[1] * PDC.raddeg
        yaw   = PDC.orientation[2] * PDC.raddeg
    else:
        roll  = PDC.orientation[3] * PDC.raddeg
        pitch = PDC.orientation[4] * PDC.raddeg
        yaw   = PDC.orientation[5] * PDC.raddeg
        
    # Create rotation matrices
    mtx_roll  = np.matrix([[1,            0,             0],
                           [0, np.cos(roll), -np.sin(roll)],
                           [0, np.sin(roll),  np.cos(roll)]])
    
    mtx_pitch = np.matrix([[ np.cos(pitch), 0, np.sin(pitch)],
                           [             0, 1,             0],
                           [-np.sin(pitch), 0, np.cos(pitch)]])
    
    mtx_yaw   = np.matrix([[np.cos(yaw), -np.sin(yaw), 0],
                           [np.sin(yaw),  np.cos(yaw), 0],
                           [          0,            0, 1]])
    
    # Pre-multiply rotation matrices
    mtx_rotation = np.matmul(mtx_yaw, np.matmul(mtx_pitch, mtx_roll))
    
    # Rotate vectors
    v = np.matmul(mtx_rotation, v)
    r0 = np.matmul(mtx_rotation, r0)
    
    # Convert back to arrays
    v = np.asarray(v).squeeze()
    r0 = np.asarray(r0).squeeze()
    
    # Translate tail vector
    if plane < 6:
        r0 += np.asarray(PDC.center_pos[0:3])
    else:
        r0 += np.asarray(PDC.center_pos[3:6])
    
    # Done
    return (tuple(r0), tuple(v))


def range_remap(x, start, end, bottom, top):
    return (((top - bottom) / (end - start)) * (x - start)) + bottom

def track_to_2d_pos(track, samples=10, steps=20, avg=5):
    """Takes as input track data for a single PDC.
    
    Returns a numpy array with shape (2,) representing
    the (x,y) coordinate of the position of the particle
    when it pierced the PDC plane.
    """

    if len(track) < 1:
        return np.zeros(2)  # if track is empty, quit

    # Pre-compute wire hit lines and drift times
    lines = np.asarray([hit_to_2d(hit) for hit in track])
    times = np.asarray([time_to_dist(hit[1]) for hit in track])

    squeeze_rate = 0.7 if steps <= 10 else (0.95 if steps >= 1000 else range_remap(steps, 10,1000, 0.6,0.95))
    
    centers = np.zeros((avg, 2))
    
    for k in range(avg):

        # Initialization
        center = np.zeros(2)  # start centered at (0,0)
        variance = np.asarray([PDC.pcbwidth / 2, PDC.pcbheight / 2])
        cur_cost = 10 ** 10

        for i in range(steps):
            costs = np.zeros(samples)
            # Distribute points within a 2d gaussian
            points = np.stack((np.random.normal(center[0], variance[0], size=(samples,)),
                               np.random.normal(center[1], variance[1], size=(samples,)))).transpose()
            # Calculate cost at each point
            for j in range(samples):
                costs[j] = sum(abs(times[k] - point_line_dist(points[j], lines[k])) for k in range(len(track)))
            # Set gaussian center to point with minimum cost
            if np.min(costs) < cur_cost:
                cur_cost = np.min(costs)
                center = points[np.argmin(costs)]
            # Decrease gaussian variance
            variance *= squeeze_rate
        centers[k] = center
        
    return np.sum(centers, axis=0) / centers.shape[0]


def track_to_heatmap(track, res=80):
    """Computes a (square) heatmap of a cost function.
    
    Returns an image with total of res^2 pixels
    """
    
    lines = [hit_to_2d(hit) for hit in track]
    times = [time_to_dist(hit[1]) for hit in track]
    
    res = int(res)  # make sure resolution is an integer
    
    heatmap = np.zeros((res, res))
    
    for i in range(res):
        for j in range(res):
            point = ( j*(PDC.pcbwidth / res) - (PDC.pcbwidth / 2) + 1/2,
                     -i*(PDC.pcbheight / res) + (PDC.pcbheight / 2) - 1/2)
            
            tot_cost = sum(abs(times[k] - point_line_dist(point, lines[k])) for k in range(len(track)))
            heatmap[i][j] = tot_cost
    
    return heatmap