"""
Contains SHMS particle drift chamber geometry data
"""

import numpy as np

# Geometry data from https://github.com/MarkKJones/hallc_replay/blob/master/PARAM/SHMS/DC/pdc_geom.param

class particle_drift_chamber(object):
    # Conversion factors
    raddeg = np.pi / 180.0
    cminch = 2.54 / 1.0

    # x, y, and z position of center for each wire chamber.
    # From survey C1771.
    center_pos = (-0.036, 0.019 - 0.43, -40.0 - 0.656,
                   0.008, 0.013 - 0.43,  40.0 - 0.668)  # centimeters

    # Roll, pitch, and yaw for each wire chamber in degrees.
    # From survey C1771.
    orientation = (180.0, -0.006, -0.006,
                   180.0,  0.001,  0.035)  # degrees

    # Wire number of center of wire chamber for each plane.
    # From technical drawings.
    central_wire = (54.25, 53.75, 40.25, 39.75, 54.25, 53.75,
                    53.75, 54.25, 39.75, 40.25, 53.75, 54.25)

    # Dimensions of the printed circuit boards (PCBs)
    pcbwidth  = 80.0  # centimeters
    pcbheight = 80.0

    # Pitch of wire chambers (distance between adjacent wires)
    pitch = 1  # centimeters

    # Number of wires in each plane
    numwires = (107, 107, 79, 79, 107, 107,
                107, 107, 79, 79, 107, 107)

    # Slope of wires in U boards (measured off horizontal axis)
    uslope = 60  # degrees
    
    # Maximum measurable drift time
    # The maximum value in file['T/P.dc.*.time'] is about 1300, so assume that this is maximum measurable drift time
    max_time = 1300.0
    
    # Minimum measurable drift time
    # The minimum value in file['T/P.dc.*.time'] is about -135, so assume that this is minimum measurable drift time
    min_time = -135.0
    
    # Empirically determined offset for U/V planes' wirenum
    # No data available to make it more accurate/precise
    wire_offset = 34.75
    
    # x position of wire chamber centers with respect to center of PDC in cm
    xpos_offset = (-0.0045,
                   -0.028,
                   0.0105,
                   0.0085,
                   -0.007,
                   -0.035,
                   -0.036,
                   0.036,
                   0.0015,
                   -0.0025,
                   -0.0045,
                   0.012)
           
    # y position of wire chamber centers with respect to center of PDC in cm
    ypos_offset = (-0.007,
                   -0.0205,
                   0.0,
                   0.0,
                   0.0045,
                   0.022,
                   -0.019,
                   0.0245,
                   0.0,
                   0.0,
                   0.0035,
                   -0.006)

    # z position in cm of each plane measured from focal plane
    # From technical drawings.
    zpos_offset = (-0.68701 * cminch,
                   -0.43701 * cminch,
                   -0.18701 * cminch,
                   0.18701 * cminch,
                   0.43701 * cminch,
                   0.68701 * cminch,
                   -0.68701 * cminch,
                   -0.43701 * cminch,
                   -0.18701 * cminch,
                   0.18701 * cminch,
                   0.43701 * cminch,
                   0.68701 * cminch)